 Spritesheets from https://kenney.nl/assets/platformer-art-extended-tileset
 Images from https://kenney.nl/assets/jumper-pack

 Thanks to Kenney for providing these high quality assets under CCO 1.0 Universal license
 https://creativecommons.org/publicdomain/zero/1.0/

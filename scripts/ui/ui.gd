extends Control 

func _ready():
	$HBoxContainer/Points.text = String(State.points)

func _process(delta):
	$HBoxContainer/Points.text = String(State.points)

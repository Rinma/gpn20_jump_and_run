extends Node2D

var velocity = 1

func _process(delta):
	position = Vector2(position.x - (200  * delta) * velocity, position.y)

func _on_Area2D_body_entered(body):
	get_tree().change_scene("res://scenes/ui/lose.tscn")
	
func _on_VisibilityEnabler2D_viewport_entered(viewport):
	$AnimatedSprite.play('walk')


func _on_WallDetector_body_entered(body):
	velocity *= -1

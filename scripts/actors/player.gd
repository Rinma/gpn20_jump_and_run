extends KinematicBody2D


export var gravity: = 3000.0
export var speed: = Vector2(300.0, 1000.0)

var velocity: = Vector2.ZERO

func _ready() -> void:
	pass
	
func _process(delta):
	if (position.y > 3000):
		get_tree().change_scene("res://scenes/ui/lose.tscn")

func _physics_process(delta: float) -> void:
	var direction = get_direction()
	velocity = calculate_move_velocity(velocity, direction)
	move_and_slide(velocity, Vector2.UP)
	play_animation()

func get_direction() -> Vector2:
	return Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		-1.0 if Input.is_action_just_pressed("jump") and is_on_floor() else 1.0
	)

func calculate_move_velocity(velocity: Vector2, direction: Vector2) -> Vector2:
	var target = velocity
	target.x = speed.x * direction.x
	target.y += gravity * get_physics_process_delta_time()
	
	if direction.y == -1.0:
		target.y = speed.y * direction.y
	
	return target

func play_animation():
	if velocity.x > 0:
		$AnimatedSprite.flip_h = false
		$AnimatedSprite.play("walk")
	elif velocity.x < 0:
		$AnimatedSprite.flip_h = true
		$AnimatedSprite.play("walk")
	else:
		$AnimatedSprite.flip_h = true
		$AnimatedSprite.play("idle")
		
	if velocity.y < 0:
		$AnimatedSprite.play("jump")
